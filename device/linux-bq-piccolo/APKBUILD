# Reference: <https://postmarketos.org/vendorkernel>
# Kernel config based on: arch/arm/configs/(CHANGEME!)

pkgname="linux-bq-piccolo"
pkgver=3.0.0
pkgrel=0
pkgdesc="bq aquaris m5 kernel fork"
arch="armv7"
_carch="arm"
_flavor="bq-piccolo"
url="https://kernel.org"
license="GPL-2.0-only"
options="!strip !check !tracedeps"
makedepends="perl sed installkernel bash gmp-dev bc linux-headers elfutils-dev devicepkg-dev gcc4"

# Compiler: latest GCC from Alpine
HOSTCC="${CC:-gcc}"
HOSTCC="${HOSTCC#${CROSS_COMPILE}}"

# Compiler: GCC 4 (doesn't compile when compiled with newer versions)
if [ "${CC:0:5}" != "gcc4-" ]; then
	CC="gcc4-$CC"
	HOSTCC="gcc4-gcc"
	CROSS_COMPILE="gcc4-$CROSS_COMPILE"
fi




# Source
_repository="android_kernel_bq_piccolo"
_commit="cm-12.1-test"
_config="config-${_flavor}.${arch}"
source="
	$pkgname-$_commit.tar.gz::https://github.com/piccolo-dev/${_repository}/archive/${_commit}.tar.gz
	$_config
"

builddir="$srcdir/${_repository}-${_commit}"

prepare() {
	default_prepare

        # Remove -Werror from all makefiles
        local i
        local makefiles="$(find . -type f -name Makefile)
                $(find . -type f -name Kbuild)"
        for i in $makefiles; do
                sed -i 's/-Werror-/-W/g' "$i"
                sed -i 's/-Werror//g' "$i"
        done

        # Prepare kernel config ('yes ""' for kernels lacking olddefconfig)
        cp "$srcdir"/$_config "$builddir"/.config
        yes "" | make ARCH="$_carch" HOSTCC="$HOSTCC" oldconfig
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS"
}

package() {
	# kernel.release
	install -D "$builddir/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"

	# zImage (find the right one)
	cd "$builddir/arch/$_carch/boot"
	_target="$pkgdir/boot/vmlinuz-$_flavor"
	for _zimg in zImage-dtb Image.gz-dtb *zImage Image; do
		[ -e "$_zimg" ] || continue
		msg "zImage found: $_zimg"
		install -Dm644 "$_zimg" "$_target"
		break
	done
	if ! [ -e "$_target" ]; then
		error "Could not find zImage in $PWD!"
		return 1
	fi
}

sha512sums="40d6ecd7b57593215c60587a00fe6f26462934b5cc26e785c880bf106982ebd4802fd83e911d518cf8e541d4c27a2f5e0dc66a345ea76e43ca079592d9bae375  linux-bq-piccolo-cm-12.1-test.tar.gz
96f4fdca2ddb71035e67a96ba447dc15b927c10a88fec20859bf37ccff1c589368a765e641d9e48d0876763b1757fc349f6c7b9ac56c98cf45273acfccc809f0  config-bq-piccolo.armv7"
