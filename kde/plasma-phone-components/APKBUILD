# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-phone-components
pkgver=0_git20191107
pkgrel=0
_commit="a1021e4d3cc5cb3b1a6ad0bdf2e3a0001b840b09"
pkgdesc="Modules providing phone functionality for Plasma"
arch="all !armhf"
url="https://www.plasma-mobile.org/"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends="plasma-nano qt5-qtquickcontrols2 plasma-workspace kactivities plasma-pa plasma-nm libqofono breeze-icons plasma-settings telepathy-ofono"
makedepends="extra-cmake-modules kpeople-dev qt5-qtdeclarative-dev kactivities-dev plasma-framework-dev kservice-dev kdeclarative-dev ki18n-dev kio-dev kcoreaddons-dev kconfig-dev kbookmarks-dev kwidgetsaddons-dev kcompletion-dev kitemviews-dev kjobwidgets-dev solid-dev kxmlgui-dev kconfigwidgets-dev kauth-dev kcodecs-dev kpackage-dev kwindowsystem-dev kdbusaddons-dev knotifications-dev kwayland-dev telepathy-qt-dev libphonenumber-dev"
source="
	$pkgname-$_commit.tar.gz::https://invent.kde.org/kde/plasma-phone-components/-/archive/$_commit/plasma-phone-components-$_commit.tar.gz
	set-postmarketos-wallpaper.patch
	startplasmamobile
	plasma-mobile.desktop
	"
options="!check" # No tests
builddir="$srcdir/$pkgname-$_commit"

build() {
	cmake \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" make install

	install -D -m755 "$srcdir"/startplasmamobile \
		"$pkgdir"/usr/bin/startplasmamobile

	install -Dm644 "$srcdir"/plasma-mobile.desktop \
		"$pkgdir"/usr/share/wayland-sessions/plasma-mobile-2.desktop
}
sha512sums="a9cb3b67370169883b018c1ece9929f170242e6ca90742bef945f17c2649cd68b7089a1b0fc9bbc0144c10419492821cd6d07fab34e68afe70e1a115c7c06b04  plasma-phone-components-a1021e4d3cc5cb3b1a6ad0bdf2e3a0001b840b09.tar.gz
5853e72077c356f8347ea1c8503d5e301505acaa39ccf2fa105abe054a3063e735f5bcb2db1c1357f53032714599bfbf687fff2cbe21a1743930b946900ff7cf  set-postmarketos-wallpaper.patch
069485a372df96f1b438c026b259bc81a6b417d59f00ef42cfd38e13f0971ce8c63db1d31c7e9603a9a5e0bfedc35ba4a1f2af041b7e24304899f40c10b87432  startplasmamobile
7f4bdbd30cda4c9e23293b7bb1eb6e8536ada056cb3bcc9a6cc3db7bbc2277eac67b519992b7e46afdf5c720df9c696b43a6a9e9f82ed7ebe3937d8c0bf4d55d  plasma-mobile.desktop"
