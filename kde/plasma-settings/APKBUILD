# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-settings
pkgver=0_git20191009
pkgrel=0
_commit="8a25b3d931dc6f7c8474f37ff42ce5e2e680f91d"
pkgdesc="Settings application for Plasma Mobile"
arch="all"
url="https://community.kde.org/Plasma/Mobile"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends="kirigami2 qt5-qtquickcontrols2 kded"
makedepends="extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev kwindowsystem-dev kcoreaddons-dev solid-dev kconfig-dev kauth-dev kdbusaddons-dev ki18n-dev kdeclarative-dev kio-dev kdelibs4support-dev karchive-dev kservice-dev kpackage-dev kconfigwidgets-dev kbookmarks-dev kcrash-dev kcompletion-dev kdesignerplugin-dev kdesignerplugin kjobwidgets-dev kdoctools-dev kemoticons-dev kguiaddons-dev kitemmodels-dev kinit-dev knotifications-dev kparts-dev kunitconversion-dev plasma-framework-dev"
source="$pkgname-$_commit.tar.gz::https://invent.kde.org/kde/plasma-settings/-/archive/$_commit/plasma-settings-$_commit.tar.gz"
options="!check" # No tests
builddir="$srcdir/$pkgname-$_commit"

prepare() {
	default_prepare

	mkdir "$builddir"/build
}

build() {
	cd "$builddir"/build
	cmake "$builddir" \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	make
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=true ctest
}

package() {
	cd "$builddir"/build
	DESTDIR="$pkgdir" make install
}
sha512sums="4dcf58d940630082208a2a0f340886a0a24a3b6542eb9711a7feab4ef99e7f24dc42950c852dd4889a2a7425981c8a1aa145c7e79fa6fcf45aeaac96bfea89d5  plasma-settings-8a25b3d931dc6f7c8474f37ff42ce5e2e680f91d.tar.gz"
